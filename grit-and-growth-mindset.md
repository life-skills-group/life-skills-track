# Grit And Growth Mindset

#### Question 1: Paraphrase (summarize) the video in a few (1 or 2) lines. Use your own words.
Answer: Grit is one of the core principles for growth. Having courage and strength of character is more significant in one's life than their talents, financial and external situations.
#### Question 2: Paraphrase (summarize) the video in a few (1 or 2) lines in your own words.
Answer: Growth mindset means to have the belief that one can develop skills and intelligence in any domain if they are motivated to put in the effort and time.
#### Question 3: What is the Internal Locus of Control? What is the key point in the video?
Answer: "Internal Locus of Control" means having the belief that one has total control over their life.
One of the key points is that having an internal locus of control is the key to staying motivated.
#### Question 4: What are the key points mentioned by speaker to build growth mindset (explanation not needed).
Answer: Key features to build growth mindset are:
1. Having belief in yourself,
2. Question your assumptions,
3. Develop a life curriculum to get what you want,
4. Honor the struggle and be resilient.

#### Question 5: What are your ideas to take action and build Growth Mindset?
Answer: Few things that I believe that are good for growth mindset are:
1. Have faith in yourself, especially in bad times,
2. Trust the process you are following,
3. Take inspiration not from the idols but from their ideologies for growth,
4. Being positive in every situation and taking setbacks constructively.