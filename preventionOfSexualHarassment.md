# Prevention of Sexual Harassment

## Q: What kinds of behaviour cause sexual harassment?
Ans: Behaviours like unwelcoming visual, verbal, non-verbal, or physical conduct of sexual nature that makes the other person uncomfortable come under sexual harassment.
## Q: What would you do in case you face or witness any incident or repeated incidents of such behaviour?
Ans: One can do the following things in case they witness sexual harassment or want to avoid such incidences:
1. Write what happened in detail,
2. Report it to assigned authorities,
3. Report the higher police officials, incase the situation is not being handled properly by the assigned authorities,
4. Support the victim in all possible ways.
5. Make sure nothing like that happens in your presence,
6. Make sure everyone around you is aware of the rules and regulations regarding sexual harassment.