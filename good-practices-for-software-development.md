# Good Practices for Software Development

### Question 1: Which point(s) were new to you?
Answer: The new point were:
* Get frequent feedback during implementation phase, so that you are on track and everyone is on the same page,
* If requirements are changed then update and inform the new deadline,
* Don't let a call go missed, pich the call and inform them to communicate after a certain period,
* Make it very easy for the person to answer your question, and mention the solutions you tried,
* Use of tools like TimeLimit, Freedom, or any other app can be helpful.

### Question 2: Which area do you think you need to improve on? What are your ideas to make progress in that area?
Answer: The areas of improvement are :
* Being more specific with my doubts,
* Being always available for real-time conversation, to make the conversation quicker and effective,
* Preservating attention specially at home,
* Deep work,
* Doing exercise daily.