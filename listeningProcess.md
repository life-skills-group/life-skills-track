# Listening and Active Communication

## 1. Active Listening

#### Question 1 - What are the steps/strategies to do Active Listening? (Minimum 6 points)
Ans - Strategies to do Active Listening are:
1. Pay attention to what the other person is saying,
2. Do not interrupt in between, and let the other person complete what they are saying,
3. Use phrases like - "tell me more about it", "what happened after that", etc. to let the other person know you are paying attention to them,
4. Use good body language to show attention toward the person you are talking to,
5. Repeat or summarize what the other person said to make sure you are on the same page,
6. Try taking notes when an important conversation is going on.

## 2. Reflective Listening
#### Question 2 - According to Fisher's model, what are the key points of Reflective Listening? (Write in your own words, use simple English)
Ans - Key points of Reflective listening are:
1. Talk less and listen more,
2. Avoid responses on lesser important things,
3. Repeat what the other person said,
4. Try to understand the underlying emotions about what the other person is trying to say,
5. Work on understanding the speaker's perspective rather than imposing our perspective on it,
6. Listen and respond with an open mind and empathy.

## 3. Reflection
#### Question 3 - What are the obstacles in your listening process?
Ans - The main obstacle in my listening process would be unable to pay attention to something for a longer duration of time.

#### Question 4 - What can you do to improve your listening?
Ans - I try to make notes during listening and sometimes try to summarize what I just heard.

## 4. Types of Communication

#### Question 5 -  When do you switch to Passive communication style in your day to day life?
Ans - I usually tend to switch to passive communication in the following scenarios:
1. Whenever I want to avoid an argument,
2. Whenever I fear of being rejected,
3. Whenever I procrastinate about a scenario.

#### Question 6 - When do you switch into Aggressive communication styles in your day to day life?
Ans - I usually tend to switch to aggressive communication in the following scenarios:
1. Whenever I feel frustrated,
2. Whenever I feel I am not being heard,
3. Whenever I feel disrespected.

#### Question 7 - When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?
Ans - I tend to switch to passive-aggressive communication whenever:
1. I get angry during a conversation,
2. I want to avoid a conflict,
3. I don't want to sound harsh.

#### Question 8 - How can you make your communication assertive? You can watch and analyse the videos, then think what would be a few steps you can apply in your own life? (Watch the videos first before answering this question.)
Ans - I would practice the following strategies to make my conversation more assertive:
1. Be more specific in my conversations,
2. Understand my needs properly,
3. Try to convey the emotions behind my objectives,
4. Practice empathy,
5. Act with composure and understand my boundaries,
6. Be more assertive with my body language.  