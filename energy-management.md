# Energy Management

### Question 1: What are the activities you do that make you relax - Calm quadrant?

Answer: Some of the activities are:
* Drink tea/coffee,
* Change the location where I was doing the task,
* Go for a walk to any place possible,
* Talk to anyone nearby,
* Listen to some soothing music.

### Question 2: When do you find getting into the Stress quadrant?

Answer: It usually happens during the following situations: 
1. Whenever I see myself not completing the task within the deadline, 
2. Compare myself to others,
3. When I have no idea how to approach the task.

### Question 3: How do you understand if you are in the Excitement quadrant?

Answer: In a situation where I have a belief that I have the control over the task or know enough to meet the end goal is where I feel that I am in the Excitement quadrant.

### Question 4: Paraphrase the Sleep is your Superpower video in your own words in brief. Only the points, no explanation.

Answer: Following are the points:
* Sleep deprivation has a deep and biological impact on our body,
* It reduces our learning capability,
* Increase the chances of Alzheimers and dementia,
* Increases aging,
* It can even lead to changes in our DNA patterns, where the good parts are deteriorated and bad parts leading to tumor, inflamation and cardiovascular diseases are incresed,
* Thus having a good sleep is not just a good habit, but a necessity for human beings,

### Question 5: What are some ideas that you can implement to sleep better?

Answer: We can enhance our sleep by-
    1. Avoiding caffine during night,
    2. Being absolutely fixed on our sleep schedule,
    3. Keeping the room temprature cooler than the normal, this allows body to relax,
    4. Avoiding afternoon naps, if sleeping at night is tough,
    5. Avoid screen time just before sleep.

### Question 6: Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points, only the points.

Answer: Following are the points:
* Physical activities have a positive impact not just on our physical body but also our brain cells,
* It improves our mood, energy, attention and learning ability,
* These activities leads to increased level of good mood neurotransmitters like dopamine, serotonin, etc.,
* Newer and more brain cells are produced through exercises/activities,
* This increases long term memory along with making our brain muscles stronger. 

### Question 7: What are some steps you can take to exercise more?

Answer: Following are the steps:
* Exercise three to four times a week for atleast 30 minutes,
* Prefer aerobic exercise to increase heart rate up,
* Fix a time for exercise,
* Make sure you enjoy it rather than taking it as a task to complete.