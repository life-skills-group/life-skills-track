# Learning Process

## How to Learn Faster with the Feynman Technique
#### Question 1: What is the Feynman Technique?
Answer: The main core of the Feynman Technique is to understand a concept so clearly that you could explain it to an amateur. If not then you need to work more on understanding the concepts yourself. 


## Learning How to Learn TED talk by Barbara Oakley
#### Question 2: In this video, what was the most interesting story or idea for you?
Answer: The concept of using a Pomodoro timer seems interesting to me. Since it allows one to effectively use time by proper cycles of focus and relaxation.

#### Question 3: What are active and diffused modes of thinking?
Answer: Active state is when you focus completely on something and let your brain go through a set pattern that is built sequentially over time.
While diffused state is a state of relaxed state that allows you to take your attention away and allows you to have a different perspective over the same topic.

## Learn Anything in 20 hours
#### Question 4: According to the video, what are the steps to take when approaching a new topic? Only mention the points
Answer: There are four steps that need to be followed when approaching a new topic:
1. Deconstruct the skill.
2. Learn enough to self-correct.
3. Remove practice barriers.
4. Practice at least 20 hours.


## Learning Principles in the Bootcamp
#### Question 5: What are some of the actions you can take going forward to improve your learning process?
Answer: I will be following these practices:
1. Use a Pomodoro timer to adequately use my time.
2. Try working on diffused modes of thinking.
3. Try to explain the concepts to an imaginary child to make sure I understand the topic thoroughly.