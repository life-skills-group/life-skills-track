# Focus Management

### Question 1: What is Deep Work?
Ans: Deep work is a state of mind when the concentration levels are so high that distractions are non-existential and that is where our brain works at its highest potential.

### Question 2: According to author how to do deep work properly, in a few points?
Ans: 
* According to the author one should work for atleast an hour or 90 minutes to actually get the deep work.
* Deadlines can also be a good factor in task completion as it allows one to removes the mental barriers of pushing tasks, 

### Question 3: How can you implement the principles in your day to day life?
Ans: We can implement the principles by doing the following:
1. Schedule your distractions: So that you could train your brain to avoid random distractions,
2. Develope a rythemic deep work ritual/pattern. Try to do deep work for 4 hours daily to build a proper set pattern for your brain. Ad hoc deep work practice is not beneficial, 
3. Evening shutdown: Write down the unfinished tasks and action plans for tomorrow and then tell yourself to shutdown and relax your brain.

### Question 4: What are the dangers of social media, in brief?
Ans: The dangers of social media are:
* It is designed to be addictive and thus takes our valuable time,
* It is affects our attention span,
* It affects us mentally as well. Feeling of isolation or loneliness is developed, and at worse it could lead to a state of depression,
* In the name of entertainment it fills our brains with unwanted data.
