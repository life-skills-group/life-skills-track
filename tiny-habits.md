# Tiny Habits

#### Question 1: In this video, what was the most interesting story or idea for you?
Answer: One of the most interesting ideas in the video was the concept of putting a tiny behavior after another existing behavior. This allows you to form a habit of doing that tiny behavior and the existing behavior acts as a trigger for it.

#### Question 2: How can you use B = MAP to make making new habits easier? What are M, A and P.
Answer: M, A, and P stands for Motivation, Ability, and Prompt respectively. In order to make new habits easier, I can:
1. Break down the habit in small steps and try to do it regularly,
2. Add that small habit after a basic task that I do regularly,
3. Add an act of celebration after doing the tiny habit.


#### Question 3: Why it is important to "Shine" or Celebrate after each successful completion of habit? (This is the most important concept in today's topic. Whatever you celebrate becomes a habit)
Answer: The concept of "Shine" or celebration is a core principle in developing a new habit as it allows one to have a sense of fulfillment and constant motivation to keep doing the task. One becomes aware of their efforts and over time that tiny task becomes a habit. 

#### Question 4: In this video, what was the most interesting story or idea for you?
Answer: There were two things that I really liked in the video:
1. The idea of having clarity of thought of what you are doing is more crucial than having motivation to do that work.
2. The concept of "Failure Premortem". It's an exercise of noticing what is holding you back. All you have to do is assume that you want to do something or achieve a goal. Now imagine that after 6 months you have failed to achieve that goal. Write down the story of how it happened and the cause of failure.

#### Question 5: What is the book's perspective about Identity?
Answer: The book insists on having an identity-based approach where one tries to have an internal congruence with the habit rather than an outcome based trigger.

#### Question 6: Write about the book's perspective on how to make a habit easier to do?
Answer: To make a habit easier to do, one can break it into smaller bits of- cue, craving, response and reward. The cue provides a trigger to start, the craving provides with motivation, the response is the very habit being performed and the reward is the end goal.

#### Question 7: Write about the book's perspective on how to make a habit harder to do?
Answer: One can make a habit harder by-
1. Making the habit invisible in your environment,
2. Making the task boring,
3. Make the task hard to perform,
4. Not adding rewards and thus making it unsatisfying.


#### Question 8: Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?
Answer: One habit that I would like to do is exercise every day to build a healthy physique. Steps that would make it a habit would be:
1. Adding a few equipments like skipping rope and dumbells in my room,
2. Start with small exercises with less frequency like 10 pushups before shower, and before every meal,
3. And I could take a picture of me every day to see the growth over time to make the process satisfying.
    

#### Question 9: Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?
Answer: A habit that I would like to eliminate is sleeping late at night. In order to avoid that I would:
1. Make a proper time schedule for the day, to manage my tasks in time,
2. Make sure to eat by 8 PM,
3. Not use or maybe switch off my phone after 10 PM,
4. Always go to bed by 11 PM and do something that calms my brain like either listening to soothing music or reading a book.
