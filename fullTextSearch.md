# Full-Text Search Databases
## Introduction
The project is going through some performance and scaling issues possibly with full-text searching. To figure out these challenges we need to look into alternative databases that focus on full-text search capabilities. Amongst several options, there are three options this paper focuses on- Lucene, Solr, and Elasticsearch.

## Elasticsearch
Elasticsearch is amongst the most popular enterprise search engines, built on top of Apache Lucene. It is a distributed, RESTful search and analytics engine well known for having strong full-text search features and instantaneous data indexing.
### Key Features
1. Distributed Architecture: Elasticsearch can handle massive amounts of data because it is made to scale horizontally.
2. Near real-time search capabilities are provided by Real-Time Search and Analytics, which is essential for applications that need access to the most recent data.
3. Easy integration with a variety of platforms and programming languages is made possible by RESTful APIs.
4. Tools for Monitoring and Management: Kibana is included for managing the Elasticsearch cluster and visualizing data.
   

## Solr
Based on Apache's free software Lucene, Solr is an open-source side project. It is based on Lucene Core and is written in Java and is one of the most widely used search platforms for integrating vertical search engines.
### Key Features
1. Distributed Search: Solr's high scalability is attributed to its support for both distributed search and index replication.
2. Configurable: Very configurable, offering a wide range of indexing and search configuration options.
3. Rich Query Syntax: Enhances search relevance and accuracy by supporting intricate queries.
4. SolrCloud: A decentralized approach for smoothly scaling and administering big search clusters.
## Lucene

Apache Lucene is a high-performance, full-featured text search engine library. It serves as the underlying technology for both Elasticsearch and Solr. It is widely used as a standard foundation for production search applications.
### Key Features
1. Customizable: Provides developers with a wide range of customization options to create specialized search solutions.
2. Lightweight: Lucene is a library that is suitable for direct application integration.
3. Flexible: Enables extensive personalization of the logic used for indexing and searching.


## Conclusion
In the end, particular project requirements, such as performance goals, scalability requirements, and the required degree of customization, should direct the choice among these technologies. All of the solutions have the potential to greatly improve full-text search performance, which makes them good choices for resolving the issues with the ongoing project.

## Reference 
1. YouTube
* [What is Elasticsearch? - IBM Technology](https://www.youtube.com/watch?v=ZP0NmfyfsoM)
* [Introduction to Apache Lucene | Edureka](https://www.youtube.com/watch?v=vLEvmZ5eEz0)
2. Google
* [What is Elasticsearch?](https://aws.amazon.com/what-is/elasticsearch/)
* [Solr - The Apache search platform](https://www.ionos.com/digitalguide/server/configuration/solr/)
* [Apache Lucene Documentation](https://lucene.apache.org/core/)
* [Introduction to Apache Lucene/Solr](https://lucidworks.com/post/introduction-to-apache-lucenesolr/)